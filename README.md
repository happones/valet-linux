# Laravel en linux

Con estas instrucciones se configurara un entorno profesional de desarollo.

**Php**

En Ubuntu y derivados superiores o iguales ala version 16.04

``$ sudo apt install php``

Verificar la version de php, deberá ser superior o igual a la 7.0

``$ php -v``

**MySql**

Instalar mysql y el cliente del mismo al momento de configurar nos pedira ingresar el password

``$ sudo apt install mysql-server mysql-client``

**Herramientas adicionales**

Para poder compartir el sitio y hacer un tunel entre nuestro equipo e internet

``$ sudo apt install network-manager libnss3-tools jq xsel``

Composer, gestor de dependecias de php

``$ sudo apt install composer``

**Configuración de Valet**

Cambiar el puerto por defecto de apache que es el 80

``$ sudo nano /etc/apache/ports.conf``

En la linea de `listen 80` sustituir el **80** por ejemplo **8088** y reiniciar apache `$ sudo service apache2 restart`

Instalar extensiones php requeridas para valet

``$ sudo apt install php*-cli php*-curl php*-mbstring php*-mcrypt php*-xml php*-zip php*-sqlite3 php*-mysql php*-pgsql``

Donde el `*` es la version de php si al momento de instalar php solo utilizamos `php` y no inidicamos la version, entonces las extensiones se instalar sin la version ejemplo `php-cli`

Activar modo debug

``$ sudo apt install php*-xdebug``

Instalar valet via composer

`$ composer global require cpriego/valet-linux`

Agregar los binarios de composer a la variable globar y poder utilizar sus comandos

`$ cd `, asegurarnos de estar en nuestra carpeta de home `~$ nano .bashrc`, al final de documento ingresar la siguiente linea `export PATH="$PATH:$HOME/.config/composer/vendor/bin"`

Verificar e instalar

`$ valet`

Deberá de arrojarnos una lista de comandos, en caso contrario volver a repetir los pasos

``$ valet install``

Una vez instalado podemos enlazar la carpeta de mis proyecto php y empezar a trabajar

Ejemplo

`$ mkdir sites && cd sites`

Dentro de sites

`$ valet park`

Instalar Laravel

`$ composer global require "laravel/installer"`

Crear un proyecto dento de la carpeta enlazada

`$ laravel new blog` esperar a que termine de crear el proyecto

Listo en el navegador escribir **blog.test** y comprobar que nuestro entorno a quedado configurado

Valet por default viene con el dominio **.test** si deseamos cambiar el dominio con el siguiente comando:

`$ valet domain dominio`, donde **dominio** es el nombre que deseamos utilizar 